<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Training</title>
    
    
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <div class="container-fluid">
        
           
        <header class="header">
            <div class="logo">
                <img src="uploads/images/yds-logo.png" alt="yds-logo" width="130" height="150">
            </div>
            
            
            <div class="login" style="float:right;">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                      Register/Login
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                      </div>
                      
                    <!--Modal-Body-->
                       <div class="modal-body">
                        <a href="register.php"> Register for Induction</a>
                        <a href="login.php"> Login </a>
                      </div>
                    <!--Modal-Body-->  
                       
                       <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>
                
                
            </div>
        </header>
        <nav>
            <ul class="nav nav-pills nav-justified">
                <li><a href="#">Home</a></li>
                <li><a href="#">About Training</a></li>
                <li><a href="#">Courses</a></li>
                <li><a href="#">Testi..</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>